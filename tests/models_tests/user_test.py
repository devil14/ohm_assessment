from tests import OhmTestCase


class UserTest(OhmTestCase):
    def test_get_multi(self):
        assert self.chuck.get_multi("PHONE") == ['+14086441234', '+14086445678']
        assert self.justin.get_multi("PHONE") == []

 	def test_get_recent_users(self):
        recent_users = User.get_recent_users()
        assert recent_users.rowcount == 3
        assert recent_users.fetchone()['tier'] == 'Silver'
