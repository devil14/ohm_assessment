from flask import jsonify, render_template, request, Response
from flask.ext.login import current_user, login_user

from functions import app
from models import User, RelUser, RelUserMulti


@app.route('/dashboard', methods=['GET'])
def dashboard():

    login_user(User.query.get(1))

@app.route('/community', methods=['GET'])
def community():
    users = User.get_recent_five_users() #List the 5 most recent users
    for user in users:
        user['phone'] = RelUserMulti.find_phone_number_by_id(user['id'])
        user['location'] = RelUser.find_loc_id(user['id'])
    args = {'users': users}
    return render_template("community.html", **args)

