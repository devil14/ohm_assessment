"""migration to do the following:
    -> Increase the point_balance for user 1 to 1000
    -> Add a location for user 2, assuming they live in the USA
    -> Change the tier for user 3 to Silver

Revision ID: 423cc0svey

Revises: 00000000
Create Date: 2018-04-28 17:39:27.308612

"""

# revision identifiers, used by Alembic.
revision = '423cc0svey'
down_revision = '00000000'

from alembic import op

def upgrade():
    op.execute(
        '''
        UPDATE user
        SET point_balance = 1000
        WHERE user_id = 1
        ''')
    op.execute(
        '''
        INSERT INTO rel_user (user_id, rel_lookup, attribute) 
        VALUES (2, 'LOCATION', 'USA')
        ''')
    op.execute(
        '''
        UPDATE user
        SET tier = 'Silver'
        WHERE user_id = 3
        '''
    )

def downgrade():
    op.execute(
        '''
        UPDATE user
        SET point_balance = 0
        WHERE user_id = 1
        '''
    )
    op.execute(
        '''
        DELETE FROM rel_user 
        WHERE user_id = 2
        '''
    )
    op.execute(
        '''
        UPDATE user
        SET tier = 'Carbon'
        WHERE user_id = 3
        '''
    )
    pass
